from flask import Flask, jsonify
from threading import Lock, Thread
import time
import numpy as np
app = Flask(__name__)

K_NUM_MACHINES = 10
K_MAX_ORDERS = 10
K_NUM_FLOORS = 10
K_FLOOR_WIDTH = 100
K_FLOOR_HEIGHT = 100
K_DELIVERY_INTERVAL = 5
K_TICK_SPEED = 0.2
K_REFIL = K_DELIVERY_INTERVAL*10
K_ZYX = [np.random.randint(0,K_NUM_FLOORS),
         np.random.randint(0,K_FLOOR_HEIGHT),
         np.random.randint(0,K_FLOOR_WIDTH)]

m_locs = np.concatenate([np.random.randint(0,K_NUM_FLOORS,size=(K_NUM_MACHINES,1)),
                         np.random.randint(0,K_FLOOR_HEIGHT,size=(K_NUM_MACHINES,1)),
                         np.random.randint(0,K_FLOOR_WIDTH,size=(K_NUM_MACHINES,1))],axis=-1).astype(np.float32)

m_difs = np.zeros_like(m_locs)
m_orders = np.random.randint(0, 3, size=(K_NUM_MACHINES,))
m_supply = np.ones_like(m_orders)*10 + np.random.normal(0,5,size=(K_NUM_MACHINES,)).astype(int)
m_lock = Lock()
m_ticks = 0

def update_machines(name):
    global m_locs, m_orders, m_ticks, m_lock, m_supply
    while True:
        m_lock.acquire()
        if m_ticks % K_DELIVERY_INTERVAL == 0:
            robots_with_orders = np.logical_and(m_orders >= 1, m_supply>=1)
            m_orders -= robots_with_orders.astype(int)
            m_supply -= robots_with_orders.astype(int)
            m_orders += np.random.geometric(p=0.7, size=(K_NUM_MACHINES,))-1
            m_orders = np.clip(m_orders,0, K_MAX_ORDERS)
            ro = np.logical_not(np.logical_and(m_orders >= 1, m_supply>=1))
            m_difs[:, 1] = (np.random.randint(0, K_FLOOR_HEIGHT, size=(K_NUM_MACHINES,))-m_locs[:,1])/float(K_DELIVERY_INTERVAL)
            m_difs[:, 2] = (np.random.randint(0, K_FLOOR_WIDTH, size=(K_NUM_MACHINES,))-m_locs[:,2])/float(K_DELIVERY_INTERVAL)
            m_difs[ro,:] = 0
        if m_ticks % K_REFIL == 0:
            m_supply += np.random.uniform(0,10,size=(K_NUM_MACHINES,)).astype(int)
        m_locs += m_difs
        m_ticks += 1
        m_lock.release()
        time.sleep(K_TICK_SPEED)


@app.route('/machines')
def get_machines():
    robos = []
    for i in np.arange(0,K_NUM_MACHINES):
        robo = {
            'id': int(i),
            'floor': str(m_locs[i,0]),
            'y': str(m_locs[i, 1]),
            'x': str(m_locs[i, 2]),
            'order_queue': int(m_orders[i]),
            'supply_level': int(m_supply[i])
        }
        robos.append(robo)
    return jsonify(robos)

@app.route('/location')
def get_location():
    loc = {
        'floor': K_ZYX[0],
        'y': K_ZYX[1],
        'x': K_ZYX[2],
    }
    return jsonify(loc)

if __name__ == '__main__':
    worker = Thread(target=update_machines, args=("Thread-1",))
    worker.start()
    app.run(port=8000)
